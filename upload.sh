#!/usr/bin/env bash
#HOW-TO RUN: REPONAME=traineeship-yum NEXUS_DNS="PutDNS"  NEXUS_USER=uploader NEXUS_PASSWORD="Put_passwd" ./upload kafka


# Pattern or list of RPM files to upload in Nexus
PACKAGES="${@:-kafka-*}"

upload_artifact(){
  local artifact="$1" repo=${REPONAME:-traineeship-yum}
  local version=$(rpm -q --queryformat %{version} -p $artifact)
  local release=$(rpm -q --queryformat %{release} -p $artifact)
  local package=$(rpm -q --queryformat %{name} -p $artifact)

  echo "Upload package $package, version $version-$release ..."
  curl -X POST -s -f -w "  Http status code: %{http_code}\n" \
       -F "r=${repo}" \
       -F "hasPom=false" \
       -F "e=rpm" \
       -F "g=traineeship.kafka.zhbanov" \
       -F "a=${package}-zhbanov" \
       -F "v=${version}-$release" \
       -F "p=rpm" \
       -F "file=@${artifact}" \
       --user "${NEXUS_USER}:${NEXUS_PASSWORD}" \
       "http://${NEXUS_DNS}:8081/nexus/service/local/artifact/maven/content"
}

echo $PACKAGES

for item in ${PACKAGES}; do
  files=$(find ./RPMS/noarch -type f -name ${item}-*.noarch.rpm)
  for file in ${files}; do
    upload_artifact $file || {
      echo >&2 "An error $? occurred while uploading artifact $file."
      exit 1
    }
  done
done
